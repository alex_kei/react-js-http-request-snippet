const ROOT_URL = 'https://swapi.dev/api/'

export default async function(path) {
  const response = await fetch(`${ROOT_URL}${path}`);
  const { status } = response;
  if (status < 200 || status >= 400) {
    throw new Error(`Request has failed. StatusCode: ${status}`);
  }
  return response.json();
}