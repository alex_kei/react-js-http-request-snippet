import React, { useState, useEffect, useCallback } from 'react';
import MoviesList from './components/MoviesList';
import AddMovie from './components/AddMovie';
import './App.css';

import apiCall from './utils/apiCall';

function App() {
  const [ movies, setMovies ] = useState([]);
  const [ isLoading, setIsLoading ] = useState(false);
  const [ errorMessage, setErrorMessage ] = useState('');

  const fetchData = useCallback(async () => {
    setIsLoading(true);
    setErrorMessage('');
    try {
      const responseBody = await apiCall('films');
      const fetchedMovies = responseBody.results.map(
        item => ({
          id: item.episode_id,
          title: item.title,
          openingText: item.opening_crawl,
          releaseDate: item.release_date,
        }),
      );
      setMovies(fetchedMovies);
    } catch (error) {
      console.log(error);
      setErrorMessage(error.message);
    } finally {
      setIsLoading(false);
    }
  }, []);

  useEffect(() => {
    fetchData();
  }, [fetchData]);

  const displayContent = () => {
    if (isLoading) {
      return 'Content is loading...';
    }
    if (errorMessage) {
      return `Error happened: ${errorMessage}`;
    }
    if (movies.length === 0) {
      return 'There are no movies for now.';
    }
    return <MoviesList movies={movies} />;
  };

  return (
    <React.Fragment>
      <section>
        <AddMovie onAddMovie={(...args) => console.log(args)}/>
      </section>
      <section>
        <button onClick={fetchData}>Fetch Movies</button>
      </section>
      <section>
        {displayContent()}
      </section>
    </React.Fragment>
  );
}

export default App;
